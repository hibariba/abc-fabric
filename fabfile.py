from fabric.api import *
from fabric.colors import green, red

def hello(name="world"):
    print green("hello"), red(name)
